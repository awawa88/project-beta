from django.db import models
from django.urls import reverse
from django.core.validators import RegexValidator


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique=True)


class Technician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.IntegerField(
        validators=[RegexValidator(r'^\d+$', message='Employee ID must contain only numeric values')],
        unique=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

    def get_api_url(self):
        return reverse("api_technicals", kwargs={"pk": self.pk})


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=300)
    status = models.CharField(max_length=50, default="Created")
    vin = models.CharField(max_length=17 )
    customer = models.CharField(max_length=100)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
        )

    def get_api_url(self):
        return reverse("api_appointments", kwargs={"pk": self.pk})
