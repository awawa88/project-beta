import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

function AutomobilesList({ automobiles, getAutomobiles }) {

  /* Initialize modal's state and behavior */
  const [showModal, setShowModal] = useState(false);
  const [selectedAutomobile, setSelectedAutomobile] = useState(null);

  const handleShowModal = (automobile) => {
    setSelectedAutomobile(automobile);
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false); //hide Modal
  };

  const handleModalAction = () => {
    deleteAutomobile(selectedAutomobile.vin);
    setShowModal(false); //hide Modal
  };


  /* Delete Automobile PUT operation */
  const deleteAutomobile = async (id) => {
    const response = await fetch(`http://localhost:8100/api/automobiles/${id}/`, {
      method: "delete",
    })
    if (response.ok) {
      getAutomobiles() //Update Automobiles
      handleCloseModal(); //Close Modal

    }
  }

  return (
    <>
      <table className="table table-striped align-middle mt-5">
        <thead>
          <tr>
            <th>Model</th>
            <th>Manufacturer</th>
            <th>Color</th>
            <th>Year</th>
            <th>VIN</th>
            <th>Picture</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {automobiles.map(automobile => {
            return (
              <tr key={automobile.vin}>
                <td>{automobile.model.name}</td>
                <td>{automobile.model.manufacturer.name}</td>
                <td>{automobile.color}</td>
                <td>{automobile.year}</td>
                <td>{automobile.vin}</td>
                <td><img src={automobile.model.picture_url} className="img-thumbnail" alt=" " width="150" height="150"></img></td>
                <td>
                  <button className="btn btn-danger" type="button" value={automobile.vin} onClick={() => handleShowModal(automobile)}>Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      {/* Modal component */}
      <Modal show={showModal} onHide={handleCloseModal}>
        <Modal.Header closeButton>
          <Modal.Title>Delete Automobile</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Are you sure you want to delete this automobile?</p>
          {selectedAutomobile && (
            <>
              <p>Model: {selectedAutomobile.model.name}</p>
              <p>Manufacturer: {selectedAutomobile.model.manufacturer.name}</p>
              <p>VIN: {selectedAutomobile.vin}</p>
            </>
          )}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseModal}>
            Close
          </Button>
          <Button variant="danger" onClick={handleModalAction}>
            Delete Automobile
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default AutomobilesList;
