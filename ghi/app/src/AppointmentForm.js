import React, { useState } from 'react';


function AppointmentForm({ getAppointments, techs }) {
    /* Set inital state for appointment form fields */
    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [date, setDate] = useState('');
    const [time, setTime] = useState('');
    const [tech, setTech] = useState('');
    const [reason, setReason] = useState('');

    const [showToast, setShowToast] = useState(false); //Initalize Toast state to hide


    /* Handle form fields change */
    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handleDateChange = (event) => {
        const value = event.target.value;
        setDate(value);
    }

    const handleTimeChange = (event) => {
        const value = event.target.value;
        setTime(value);
    }

    const handleTechChange = (event) => {
        const value = event.target.value;
        setTech(value);
    }

    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }


    /*Handle Create appointment */
    const handleSubmit = async (event) => {
        event.preventDefault();

        const datetime = new Date(date + " " + time).toISOString(); //combine Date and Time into datetime


        /* Create appointment data object */
        const data = {};
        data.vin = vin;
        data.reason = reason;
        data.customer = customer;
        data.technician = tech;
        data.date_time = datetime

        /* Post appointment data to the Appointments API */
        const appointmentsUrl = "http://localhost:8080/api/appointments/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        }

        const response = await fetch(appointmentsUrl, fetchConfig);

        if (response.ok) {

            /* Reset form fields */
            setVin('');
            setCustomer('');
            setTech('');
            setReason('');
            setDate('');
            setTime('');
            document.getElementById("time").selectedIndex = 0;
            document.getElementById("date").selectedIndex = 0;

            getAppointments(); // Update Appointments

            setShowToast(true); // Show Toast message
            setTimeout(() => setShowToast(false), 3000); // Hide Toast After 3 secs
        }

    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Service Appointment</h1>
                    { /*Form Fields */}
                    <form onSubmit={handleSubmit} id="create-appointment-form">
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleVinChange}
                                placeholder="Automobile VIN"
                                required
                                type="text"
                                name="vin"
                                id="vin"
                                className="form-control"
                                value={vin}
                            />
                            <label htmlFor="VIN">VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleCustomerChange}
                                placeholder="Customer"
                                required
                                type="text"
                                name="customer"
                                id="customer"
                                className="form-control"
                                value={customer}
                            />
                            <label htmlFor="Customer">Customer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleDateChange}
                                placeholder="Date"
                                required
                                type="Date"
                                name="date"
                                id="date"
                                className="form-control"
                                value={date}
                            />
                            <label htmlFor="Date">Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleTimeChange}
                                placeholder="Employee ID"
                                required
                                type="time"
                                name="time"
                                id="time"
                                className="form-control"
                                value={time}
                            />
                            <label htmlFor="Time">Time</label>
                        </div>
                        <select onChange={handleTechChange} name="tech" id="tech" className="form-select" required value={tech}>
                            <option value="">Technician</option>
                            {techs.map(tech => {
                                return (
                                    <option key={tech.id} value={tech.employee_id}>{tech.first_name} {tech.last_name} ID# ({tech.employee_id})</option>
                                )
                            })}
                        </select>
                        <p></p>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleReasonChange}
                                placeholder="Reason"
                                required
                                type="text"
                                name="reason"
                                id="reason"
                                className="form-control"
                                value={reason}
                            />
                            <label htmlFor="Reason">Reason</label>
                        </div>

                        {/* Create button  */}
                        <button className="btn btn-primary">Create</button>


                        {/* Toast component */}
                        <div
                            className={`toast align-items-center text-white bg-success border-0 position-fixed bottom-0 end-0 m-3 ${showToast ? 'show' : ''
                                }`}
                            role="alert"
                            aria-live="assertive"
                            aria-atomic="true"
                        >
                            <div className="d-flex">
                                <div className="toast-body">Appointment created successfully!</div>
                                <button
                                    type="button"
                                    className="btn-close btn-close-white me-2 m-auto"
                                    data-bs-dismiss="toast"
                                    aria-label="Close"
                                    onClick={() => setShowToast(false)}
                                ></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default AppointmentForm;
