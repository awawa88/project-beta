import React, { useState } from 'react'


function ManufacturersForm({ getManufacturers }) {

    const [manufacturer, setManufacturer] = useState('');

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = manufacturer;

        const manufacturersUrl = "http://localhost:8100/api/manufacturers/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        }

        const response = await fetch(manufacturersUrl, fetchConfig);

        if (response.ok) {

            const createManufacturer = await response.json();
            setManufacturer('');

            getManufacturers();
        }


    }



    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Manufacturer</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleManufacturerChange}
                                placeholder="Manufacturer name"
                                required
                                type="text"
                                name="manufacturer"
                                id="manufacturer"
                                className="form-control"
                                value={manufacturer}
                            />
                            <label htmlFor="manufacturer">Manufacturer name</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ManufacturersForm;
