import React, { useState } from 'react'


function TechnicianForm({ getTechs }) {

    const [employeeId, setEmployeeId] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');

    const handleIdChange = (event) => {
        const value = event.target.value;
        setEmployeeId(value);
    }

    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.employee_id = employeeId;
        data.first_name = firstName;
        data.last_name = lastName;

        const techsUrl = "http://localhost:8080/api/technicians/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        }

        const response = await fetch(techsUrl, fetchConfig);

        if (response.ok) {
            setEmployeeId('');
            setFirstName('');
            setLastName('');
            getTechs();
        }

    }


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Technician</h1>
                    <form onSubmit={handleSubmit} id="create-tech-form">
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleFirstNameChange}
                                placeholder="First Name"
                                required
                                type="text"
                                name="firstName"
                                id="firstName"
                                className="form-control"
                                value={firstName}
                            />
                            <label htmlFor="First Name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleLastNameChange}
                                placeholder="Last Name"
                                required
                                type="text"
                                name="lastName"
                                id="lastName"
                                className="form-control"
                                value={lastName}
                            />
                            <label htmlFor="Last Name">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleIdChange}
                                placeholder="Employee ID"
                                required
                                type="number"
                                name="employeeId"
                                id="employeeId"
                                className="form-control"
                                value={employeeId}
                            />
                            <label htmlFor="Employee ID">Employee ID</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default TechnicianForm;
