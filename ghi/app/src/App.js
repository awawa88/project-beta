import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useEffect, useState } from 'react';
import MainPage from './MainPage';
import ManufacturersList from "./ManufacturersList";
import ManufacturersForm from './ManufacturerForm';
import ModelList from './ModelList';
import Nav from './Nav';
import AutomobilesList from './AutomobilesList'
import CreateModelForm from './CreateModelForm'
import AutomobileForm from './AutomobileForm'
import SalespersonForm from './SalespersonForm';
import NewSalesForm from './NewSalesForm';
import SalesList from './SalesList';
import CustomerForm from './CreateCustomerForm';
import SalesPeopleList from './SalesPeopleList';
import CustomerList from './CustomerList';
import SalesPersonHistory from './SalesPersonHistory';
import TechniciansList from './TechniciansList';
import TechnicianForm from './TechnicianForm';
import AppointmentForm from './AppointmentForm';
import AppointmentsList from './AppointmentsLists';
import AppointmentHistory from './AppointmentHistory';

function App() {
  const [manufacturers, setManufacturers] = useState([]);
  const [vehicleModels, setVehicleModels,] = useState([]);
  const [automobiles, setAutomobiles] = useState([]);
  const [salespersons, setSalespersons] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [sales, setSales] = useState([]);
  const [techs, setTechs] = useState([]);
  const [appointments, setAppointments] = useState([]);
  const [automobileVOs, setautomobileVOs] = useState('');

  const getAutomobileVOs = async () => {
    const AutomobileVOsUrl = "http://localhost:8080/api/automobileVO/";

    const AutomobileVOsResponse = await fetch(AutomobileVOsUrl);

    if (AutomobileVOsResponse.ok) {
      const data = await AutomobileVOsResponse.json();
      const automobileVOs = data.automobileVO;
      setautomobileVOs(automobileVOs);
    }
  }

  const getManufacturers = async () => {
    const manufacturersUrl = "http://localhost:8100/api/manufacturers";

    const manufacturersResponse = await fetch(manufacturersUrl);

    if (manufacturersResponse.ok) {
      const data = await manufacturersResponse.json();
      const manufacturers = data.manufacturers;
      setManufacturers(manufacturers);
    }
  }

  const getVehicleModels = async () => {
    const vehicleModelsUrl = "http://localhost:8100/api/models/";

    const vehicleModelsResponse = await fetch(vehicleModelsUrl);

    if (vehicleModelsResponse.ok) {
      const data = await vehicleModelsResponse.json();
      const vehicleModels = data.models;
      setVehicleModels(vehicleModels);
    }
  }

  const getAutomobiles = async () => {
    const automobilesUrl = "http://localhost:8100/api/automobiles/";

    const automobilesResponse = await fetch(automobilesUrl);

    if (automobilesResponse.ok) {
      const data = await automobilesResponse.json();
      const automobiles = data.autos;
      setAutomobiles(automobiles);
    }
  };

  const getSalespersons = async () => {
    const response = await fetch('http://localhost:8090/api/salespeople/');
    if (response.ok) {
      const data = await response.json();
      const salespersons = data.salespersons
      setSalespersons(salespersons)
    }
  }

  const getCustomer = async () => {
    const response = await fetch('http://localhost:8090/api/customers/');
    if (response.ok) {
      const data = await response.json();
      const customers = data.Customer
      setCustomers(customers)
    }
  }

  const getSales = async () => {
    const response = await fetch('http://localhost:8090/api/sales/');
    if (response.ok) {
      const data = await response.json();
      const sales = data.Sales
      setSales(sales)
    }
  }

  const getAppointments = async () => {
    const appointmentsUrl = "http://localhost:8080/api/appointments/";

    const appointmentsResponse = await fetch(appointmentsUrl);

    if (appointmentsResponse.ok) {
      const data = await appointmentsResponse.json();
      const appointments = data.appointments;

      setAppointments(appointments);
    }
  }

  const getTechs = async () => {
    const techsUrl = "http://localhost:8080/api/technicians/";

    const techsResponse = await fetch(techsUrl);

    if (techsResponse.ok) {
      const data = await techsResponse.json();
      const techs = data.technicians;

      setTechs(techs);
    }
  }


  useEffect(() => { getManufacturers(); getVehicleModels(); getAutomobiles(); getSalespersons(); getSales(); getCustomer(); getTechs(); getAppointments(); getAutomobileVOs(); }, []);

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/manufacturers" element={<ManufacturersList manufacturers={manufacturers} />} />
          <Route path="/manufacturers/new" element={<ManufacturersForm getManufacturers={getManufacturers} />} />
          <Route path="/models" element={<ModelList vehicleModels={vehicleModels} />} />
          <Route path="/models/new" element={<CreateModelForm getVehicleModels={getVehicleModels} manufacturers={manufacturers} />} />
          <Route path="/automobiles" element={<AutomobilesList getAutomobiles={getAutomobiles} automobiles={automobiles} />} />
          <Route path="/automobiles/new" element={<AutomobileForm getAutomobiles={getAutomobiles} vehicleModels={vehicleModels} />} />
          <Route path="salespersons/" element={<SalespersonForm salespersons={salespersons} getSalespersons={getSalespersons} />} />
          <Route path="salesperson" element={<SalesPeopleList />} />
          <Route path='sales'>
            <Route path='new' element={<NewSalesForm getSales={getSales} getAutomobiles={getAutomobiles} getCustomer={getCustomer} getSalespersons={getSalespersons} />} />
            <Route path='' element={<SalesList sales={sales} />} />
            <Route path='history' element={<SalesPersonHistory />} />
          </Route>
          <Route path="customers" >
            <Route path='' element={<CustomerList customers={customers} />} />
            <Route path='new' element={<CustomerForm getCustomer={getCustomer}/>} />
          </Route>
          <Route path="/technicians" element={<TechniciansList techs={techs} />} />
          <Route path="/technicians/new" element={<TechnicianForm getTechs={getTechs} techs={techs} />} />
          <Route path="/appointments" element={<AppointmentsList getAppointments={getAppointments} getAutomobileVOs={getAutomobileVOs} appointments={appointments} automobileVOs={automobileVOs} />} />
          <Route path="/appointments/new" element={<AppointmentForm getAppointments={getAppointments} appointments={appointments} techs={techs} />} />
          <Route path="/history" element={<AppointmentHistory appointments={appointments} automobiles={automobiles} getAutomobileVOs={getAutomobileVOs} automobileVOs={automobileVOs}  />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}




export default App;
