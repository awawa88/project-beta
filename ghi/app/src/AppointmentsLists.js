import React, { useState, useEffect } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

function AppointmentsList({ appointments, getAppointments, automobileVOs, getAutomobileVOs}) {




    //Initialize modal's state and behavior.
    const [showModal, setShowModal] = useState(false);
    const [modalType, setModalType] = useState('');
    const [selectedAppointment, setSelectedAppointment] = useState(null);

    const handleShowModal = (type, appointment) => {
        setModalType(type);
        setSelectedAppointment(appointment);
        setShowModal(true);
    };

    const handleCloseModal = () => {
        setShowModal(false);
    };

    const handleModalAction = () => {
        if (modalType === 'cancel') {
            cancelAppointment(selectedAppointment);
        } else if (modalType === 'finish') {
            finishAppointment(selectedAppointment);
        }
        setShowModal(false);
    };


    /* This function turns dataTime string into date and time string variable */
    const formatDateTime = (dateTimeString) => {

        const appointmentDateTime = new Date(dateTimeString);
        const formattedDate = appointmentDateTime.toLocaleDateString();
        const formattedTime = appointmentDateTime.toLocaleTimeString();

        return { formattedDate, formattedTime };
    };


    /* Cancel Appointment PUT operation */
    const cancelAppointment = async (appointment) => {

        const appointmentsUrl = `http://localhost:8080/api/appointments/${appointment.id}/cancel`;
        const fetchConfig = {
            method: 'PUT',
        };
        const response = await fetch(appointmentsUrl, fetchConfig);
        if (response.ok) {
            getAppointments();
        }
    };

    /* Delete Appointment PUT operation */
    const finishAppointment = async (appointment) => {

        const appointmentsUrl = `http://localhost:8080/api/appointments/${appointment.id}/finish`;
        const fetchConfig = {
            method: 'PUT',
        };
        const response = await fetch(appointmentsUrl, fetchConfig);
        if (response.ok) {
            getAppointments();
        }
    };

    useEffect(() => { getAutomobileVOs(); }, []);

    return (
        <div>
            <h1>Service Appointment</h1>

            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Is VIP?</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map((appointment) => {
                        if (appointment.status === 'Finished' || appointment.status === 'Cancelled') {
                            return null;
                        }

                        //Assigning formatted Date and formatted Time from
                        const { formattedDate, formattedTime } = formatDateTime(
                            appointment.date_time
                        );

                        //Use a ternary operator to find matching vin in the autobiles value object. If it matches, return Yes (VIP), otherwise No (VIP).
                        const isVIP = automobileVOs.find(automobile => automobile.vin === appointment.vin) ? "Yes" : "No";

                        return (
                            <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{isVIP}</td>
                                <td>{appointment.customer}</td>
                                <td>{formattedDate}</td>
                                <td>{formattedTime}</td>
                                <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                                <td>{appointment.reason}</td>
                                <td>
                                    <button
                                        id={appointment.id}
                                        className="btn btn-danger"
                                        onClick={() => handleShowModal('cancel', appointment)}
                                        type="button"
                                    >
                                        Cancel
                                    </button>
                                </td>
                                <td>
                                    <button
                                        id={appointment.id}
                                        className="btn btn-success"
                                        onClick={() => handleShowModal('finish', appointment)}
                                        type="button"
                                    >
                                        Finish
                                    </button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
                    {/* Modal component */}
            <Modal show={showModal} onHide={handleCloseModal}>
                <Modal.Header closeButton>
                    <Modal.Title>{modalType === 'cancel' ? 'Cancel Appointment' : 'Finish Appointment'}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <p>Are you sure you want to {modalType} this appointment?</p>
                    {selectedAppointment && (
                        <>
                            <p>VIN: {selectedAppointment.vin}</p>
                            <p>Customer: {selectedAppointment.customer}</p>
                            <p>Date: {formatDateTime(selectedAppointment.date_time).formattedDate}</p>
                        </>
                    )}

                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleCloseModal}>
                        Close
                    </Button>
                    <Button variant={modalType === 'cancel' ? 'danger' : 'success'} onClick={handleModalAction}>
                        {modalType === 'cancel' ? 'Cancel Appointment' : 'Finish Appointment'}
                    </Button>
                </Modal.Footer>
            </Modal>
        </div>
    );
}

export default AppointmentsList;
