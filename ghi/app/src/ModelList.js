function ModelList({vehicleModels}) {

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Manufacturer</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
                {vehicleModels.map((vehicleModel) => {
                    return (
                        <tr key={vehicleModel.id}>
                            <td>{vehicleModel.name}</td>
                            <td>{vehicleModel.manufacturer.name}</td>
                            <td><img src={vehicleModel.picture_url} alt={vehicleModel.name} height="80px" /></td>


                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default ModelList;
